package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.function.Consumer;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public abstract class JpaTestBase {
    @Autowired
    private EntityManager entityManager;

    protected void clear(Consumer<EntityManager> action) {
        action.accept(entityManager);
        entityManager.clear();
    }

    protected void clear(Runnable action) {
        clear(em -> action.run());
    }

    protected void run(Consumer<EntityManager> action) {
        action.accept(entityManager);
    }

    protected void run(Runnable action) {
        run(em -> action.run());
    }
}
